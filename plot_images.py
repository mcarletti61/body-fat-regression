# -*- coding: utf-8 -*-

import argparse

import pickle
import numpy as np
import os

import matplotlib.pyplot as plt

#%% ----------------------------------------------------------------------------

def show_history(hist, idx, title='History', show_immediately=False, save_figure=False):
    '''Create a figure showing the training/testing history accordingly to \'idx\'.
    '''
    
    nb_epochs = hist.shape[0]

    # extract losses
    train_hist = []
    test_hist = []
    for ep in range(nb_epochs):
        train_hist.append(hist[ep,0,idx])
        test_hist.append(hist[ep,1,idx])
    train_hist = np.asarray(train_hist)
    test_hist = np.asarray(test_hist)

    # draw figure
    plt.figure()
    xx = np.linspace(1, nb_epochs, nb_epochs)
    plt.plot(xx, train_hist, color='b', label='train')
    plt.plot(xx, test_hist, color='r', label='test')
    plt.title(title)
    plt.xlabel('Epochs')
    plt.ylabel('Value')
    plt.grid(True)
    plt.legend()

    if save_figure:
        fname = os.path.join(args.model_dir, title + '.png')
        plt.savefig(fname)

    if show_immediately:
        plt.show()

def show_prediction(y_pred, y_targ, title='Predictions', sort_res=False, show_immediately=False, save_figure=False):
    '''Create a figure showing the predictions comparison.
    '''
    
    if sort_res:
        idx = np.argsort(y_targ)
        y_targ = y_targ[idx]
        y_pred = y_pred[idx]

    # draw figure
    plt.figure()
    xx = np.linspace(1, y_pred.size, y_pred.size)
    plt.plot(xx, y_targ, color='g', label='targets')
    plt.plot(xx, y_pred, color='b', label='predictions')
    plt.title(title)
    plt.xlabel('Samples')
    plt.ylabel('Prediction')
    plt.grid(True)
    plt.legend()

    if save_figure:
        fname = os.path.join(args.model_dir, title + '.png')
        plt.savefig(fname)

    if show_immediately:
        plt.show()

# show bland-altman plot of predictions
def bland_altman_plot(data1, data2, title='Bland-Altman plot', show_immediately=False, save_figure=False, *params, **kwparams):
    '''Create the Bland-Altman plot of the input vectors.
    '''

    mean  = np.mean([data1, data2], axis=0)
    diff  = data1 - data2        # Difference between data1 and data2
    md    = np.mean(diff)        # Mean of the difference
    sd    = np.std(diff, axis=0) # Standard deviation of the difference

    plt.figure()
    plt.title(title)
    plt.xlabel('Mean')
    plt.ylabel('Diff')
    plt.scatter(mean, diff, *params, **kwparams)
    plt.axhline(md,           color='gray', linestyle='--')
    plt.axhline(md + 1.96*sd, color='gray', linestyle='--')
    plt.axhline(md - 1.96*sd, color='gray', linestyle='--')

    if save_figure:
        fname = os.path.join(args.model_dir, title + '.png')
        plt.savefig(fname)

    if show_immediately:
        plt.show()

if __name__ == '__main__':

    seed = 23092017
    np.random.seed(seed)

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--model_dir', type=str, required=True,
                        help='results folder path')           
    parser.add_argument('--history', action='store_true',
                        help='show train and/or testing loss/score over epochs')
    parser.add_argument('--predictions', action='store_true',
                        help='show train and/or testing predictions')
    parser.add_argument('--bland_altman', action='store_true',
                        help='show train and/or testing bland-altman plot')
    parser.add_argument('--train', action='store_true',
                        help='show train data')
    parser.add_argument('--test', action='store_true',
                        help='show test data')
    parser.add_argument('--sort', action='store_true',
                        help='sort predictions by target values')
    parser.add_argument('--save_figures', action='store_true',
                        help='save figures in model_dir folder')
    parser.add_argument('--show_figures', action='store_true',
                        help='plot figures')
    parser.add_argument('--verbose', action='store_true',
                        help='print additional information')

    args = parser.parse_args()

    assert os.path.isdir(args.model_dir)
    assert args.history or args.predictions or args.bland_altman
    assert args.train or args.test

    if args.verbose:
        print(args)

    #%% ----------------------------------------------------------------------------

    # Load results
    with open(os.path.join(args.model_dir, 'results.pickle'), 'rb') as fp:
        data = pickle.load(fp)

    history = data['history']
    train_preds = data['train_preds']
    train_targets = data['train_targets']
    test_preds = data['test_preds']
    test_targets = data['test_targets']

    # Plot learning history
    if args.history:
        if args.train:
            show_history(history, 0, 'Smooth-L1 Loss', save_figure=args.save_figures)
        if args.test:
            show_history(history, 1, 'Mean Absolute Error', save_figure=args.save_figures)

    # Plot predictions
    if args.predictions:
        if args.train:
            show_prediction(train_preds, train_targets, 'Train predictions', sort_res=args.sort, save_figure=args.save_figures)
        if args.test:
            show_prediction(test_preds, test_targets, 'Test predictions', sort_res=args.sort, save_figure=args.save_figures)

    # Bland-Altman-plot
    if args.bland_altman:
        if args.train:
            bland_altman_plot(train_preds, train_targets, 'Train Bland-Atlman plot', save_figure=args.save_figures)
        if args.test:
            bland_altman_plot(test_preds, test_targets, 'Test Bland-Atlman plot', save_figure=args.save_figures)

    if args.show_figures:
        plt.show()
