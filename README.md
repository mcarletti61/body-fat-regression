# Towards Fat Percentage Estimation from Depth Sensor Data

[M. Carletti](http://marcocarletti.altervista.org/), [M. Cristani](http://profs.sci.univr.it/~cristanm/), V. Cavedon, C. Milanese, C. Zancanaro, [A. Giachetti](http://www.andreagiachetti.it/andrea/)

Project @ [author's page](http://marcocarletti.altervista.org/publications/fat-estimation-from-depth/)

Paper @ [COMING.SOON]()

---

## Requirements

- [PyTorch](http://pytorch.org/) and torchvision
- Python modules: numpy, matplotlib, pillow, scipy [_io, misc, ndimage_], glob

---

## Usage

To have a complete knowledge about the parameters and their default values, look at the argument list in each python file called by the following bash scripts.

### Generate binary datasets

From the root folder of the project:

```
cd data/FatnetBodyScan
python3 gendata.py
```
```
cd data/FatnetBodyScan
python3 gendata.py
```

### Train front and back view of a dataset

```
./run_train DATASET [GPU_ID]
```

```DATASET``` must be 'HeadLegless', 'Full', 'HeadLegArmless', 'Headless', 'Original'.

```GPU_ID``` is a positive integer; if not specified, PyTorch will decide the GPU to use.

This will take a long time depending on your hardware. We tested this code on a NVIDIA GTX 1080 that takes less than 10 minutes per dataset (front or rear view).

At the end, the output folder will contain a folder for each dataset view. Each of these folders contains:
- model.pth (PyTorch model saved with ```torch.load``` function)
- results.pickle (contains the results dictionary)
- info.txt (list of parameters used for training)
- x6 images showing the loss/score history, train/test predictions and train/test Bland-Altman plots

If you prefer to convert the results as mat files, run the script ```convert_res_to_max.py``` from the root folder.

### Test front, back and combined views

```
./run_test DATASET [GPU_ID]
```

It will print the results on terminal.

### Generate saliency maps

```
./run_saliency DATASET [GPU_ID]
```

Generates a folder containing the summary views of the dataset.