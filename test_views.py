# -*- coding: utf-8 -*-

import argparse

import torch
from torch.autograd import Variable
from torchvision.models import resnet50
import torchvision.transforms as transforms

import numpy as np
import sys
import os

from utils import numpy_to_torch, torch_to_numpy, find_outliers
from metrics import mean_absolute_error
from FATDATA import FATDATA

from test import test
from plot_images import show_prediction

if __name__ == '__main__':

    seed = 23092017
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--first_view_dir', type=str, required=True,
                        help='results folder path')
    parser.add_argument('--second_view_dir', type=str, required=True,
                        help='results folder path')
    parser.add_argument('--input_shape', type=int, default=224,
                        help='input image resolution')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='number of samples per batch')
    parser.add_argument('--gpu_id', type=int, default=None,
                        help='enable cuda and use the specified gpu')
    parser.add_argument('--use_cuda', action='store_true',
                        help='enable cuda; if enabled and \'gpu_id\' is not set, the first available gpu will be used by default')
    parser.add_argument('--remove_outliers', action='store_true',
                        help='compute testing results removing outliers')
    parser.add_argument('--verbose', action='store_true',
                        help='print additional information')

    args = parser.parse_args()

    assert os.path.isdir(args.first_view_dir)
    assert os.path.isdir(args.second_view_dir)

    if args.gpu_id is not None:
        args.use_cuda = True
    
    if args.use_cuda:
        if args.gpu_id is None:
            args.gpu_id = 0

    if args.verbose:
        print(args)

    #%% Load model -----------------------------------------------------------------

    # Normalize RGB-8 images between -1 and +1.
    transf = transforms.Compose([transforms.Scale((args.input_shape,args.input_shape)),
                                transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    train_predictions = []
    train_targets = []
    test_predictions = []
    test_targets = []

    model_dirs = [args.first_view_dir, args.second_view_dir]

    for md in model_dirs:

        model_name = os.path.join(md, 'model.pth')
        # we assume the name of the model folder follows the format in the train.py script
        # that is, the first part of the folder name is the name of the FATDATA dataset
        b = os.path.basename(md[:-1]) if md[-1] == '/' else os.path.basename(md)
        dsname = '_'.join(b.split('_')[0:1])
        
        if args.verbose:
            print('Loading dataset:', dsname)
        trainset = FATDATA(name=dsname, train=True, transform=transf)
        testset = FATDATA(name=dsname, train=False, transform=transf)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=False, num_workers=1)
        testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=1)  
    
        if args.verbose:
            print('Loading model:', model_name)
        model = torch.load(model_name)
        model.eval()
        
        loss_fun = torch.nn.SmoothL1Loss()
        score_fun = mean_absolute_error
        
        if args.use_cuda:
            model.cuda(args.gpu_id)
            loss_fun.cuda(args.gpu_id)
        
        # Testing -------------------------
        if args.verbose:
            print('Testing...')
        
        try:
            preds, targets, loss, score = test(model, trainloader, loss_fun, score_fun, use_cuda=args.use_cuda, gpu_id=args.gpu_id)
            train_predictions.append(preds)
            train_targets.append(targets)

            preds, targets, loss, score = test(model, testloader, loss_fun, score_fun, use_cuda=args.use_cuda, gpu_id=args.gpu_id)
            test_predictions.append(preds)
            test_targets.append(targets)
            
        except Exception as e:
            print(e.with_traceback, e.args)

    train_predictions = np.asarray(train_predictions).swapaxes(0,1)
    train_targets= np.asarray(train_targets).swapaxes(0,1)
    test_predictions = np.asarray(test_predictions).swapaxes(0,1)
    test_targets = np.asarray(test_targets).swapaxes(0,1)

    #%% Compute final scores with outliers -----------------------------------------

    print('#' * 60)
    print('Result considering outliers')

    train_front_mae = mean_absolute_error(train_predictions[:,0], train_targets[:,0])
    train_back_mae = mean_absolute_error(train_predictions[:,1], train_targets[:,1])
    print('Train MAEs: [f] {:.3f} [b] {:.3f}'.format(train_front_mae, train_back_mae))

    wf = 1. - train_front_mae / np.sum(train_front_mae + train_back_mae)
    wb = 1. - train_back_mae / np.sum(train_front_mae + train_back_mae)
    print('Normalized errors: [f] {:.3f} [b] {:.3f}'.format(wf, wb))

    test_front_mae = mean_absolute_error(test_predictions[:,0], test_targets[:,0])
    test_back_mae = mean_absolute_error(test_predictions[:,1], test_targets[:,1])
    print('Test MAEs: [f] {:.3f} [b] {:.3f}'.format(test_front_mae, test_back_mae))

    print('*' * 60)

    simple_mean = test_predictions[:].mean(axis=1)
    e1 = mean_absolute_error(simple_mean, test_targets[:,0])
    print('Test mean score [simple]: {:.3f}'.format(e1))

    weighted_mean = (test_predictions[:] * np.asarray([wf, wb])).sum(axis=1)
    e2 = mean_absolute_error(weighted_mean, test_targets[:,0])
    print('Test mean score [weighted]: {:.3f}'.format(e2))

    #show_prediction(simple_mean, test_targets[:,0], title='Combined test prediction (simple)', sort_res=True, show_immediately=True)
    #show_prediction(weighted_mean, test_targets[:,0], title='Combined test prediction (weighted)', sort_res=True, show_immediately=True)

    if args.remove_outliers:
        #%% Compute outliers -----------------------------------------------------------

        train_out_idx_front = find_outliers(train_predictions[:,0] - train_targets[:,0])
        train_valid_idx_front = np.array([True] * train_targets.shape[0])
        train_valid_idx_front[train_out_idx_front] = False

        train_out_idx_back = find_outliers(train_predictions[:,1] - train_targets[:,1])
        train_valid_idx_back = np.array([True] * train_targets.shape[0])
        train_valid_idx_back[train_out_idx_back] = False

        test_out_idx_front = find_outliers(test_predictions[:,0] - test_targets[:,0])
        test_valid_idx_front = np.array([True] * test_targets.shape[0])
        test_valid_idx_front[test_out_idx_front] = False

        test_out_idx_back = find_outliers(test_predictions[:,1] - test_targets[:,1])
        test_valid_idx_back = np.array([True] * test_targets.shape[0])
        test_valid_idx_back[test_out_idx_back] = False

        #%% Compute final scores without outliers --------------------------------------

        print('#' * 60)
        print('Result without outliers')

        train_front_mae = mean_absolute_error(train_predictions[train_valid_idx_front,0], train_targets[train_valid_idx_front,0])
        train_back_mae = mean_absolute_error(train_predictions[train_valid_idx_back,1], train_targets[train_valid_idx_back,1])
        print('Train MAEs: [f] {:.3f} [b] {:.3f}'.format(train_front_mae, train_back_mae))

        wf = 1. - train_front_mae / np.sum(train_front_mae + train_back_mae)
        wb = 1. - train_back_mae / np.sum(train_front_mae + train_back_mae)
        print('Normalized errors: [f] {:.3f} [b] {:.3f}'.format(wf, wb))

        print('*' * 60)

        test_front_mae = mean_absolute_error(test_predictions[test_valid_idx_front,0], test_targets[test_valid_idx_front,0])
        test_back_mae = mean_absolute_error(test_predictions[test_valid_idx_back,1], test_targets[test_valid_idx_back,1])
        print('Test MAEs: [f] {:.3f} [b] {:.3f}'.format(test_front_mae, test_back_mae))
