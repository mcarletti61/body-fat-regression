import numpy as np
import glob
import os
from PIL import Image
import scipy.io as sio
import xlrd

np.random.seed(23092017)

target_shape = (224,224)
nb_channels = 3

workbook = xlrd.open_workbook('raw_data/Chievo Marzo 2017.xlsx')
sheet = workbook.sheet_by_index(1)

gtvals = []
for r in range(sheet.nrows):
    line = sheet.row_values(r)
    if line[0] != '':
        gtvals.append(line[-1])
gtvals = np.asarray(gtvals)

def read_depth_images(dname):
    
    fnames = glob.glob('raw_data/' + dname + '/**/*.png', recursive=True)
    fids = np.array([int(x) - 1 for x in [os.path.basename(os.path.dirname(x)) for x in fnames]])

    nb_samples = fids.size
    target_shape = (224, 224)

    imshape = target_shape + (nb_channels,)

    images = np.zeros((nb_samples,) + imshape, dtype=np.uint8)
    values = np.zeros((nb_samples,), dtype=np.float32)

    count = 0
    for f,i in zip(fnames, fids):
        try:
            # load and resize the image
            image = Image.open(f)
            if image.size != target_shape:
                image = image.resize(target_shape, Image.NEAREST)
            image = np.stack(nb_channels * [image])
            image = np.transpose(image, (1, 2, 0)) # HWC
            images[count] = image
            values[count] = gtvals[i]
            count += 1
        except Exception as e:
            print('Skipping', f, 'because', e)
    
    images = images[:count]
    values = values[:count]
    nb_samples = count

    idx = np.argsort(values)
    images = images[idx]
    values = values[idx]

    print('Loaded', nb_samples, 'depth images')

    return images, values

def save_dataset(fname, data):
    print('Saving', fname, 'as mat file...')
    variables = {}
    variables['images'] = data[0]
    variables['values'] = data[1]

    dest_folder = './bin_data/'
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)
    sio.savemat(dest_folder + fname + '.mat', variables, do_compression=True)

dnames = ['Full', 'HeadLegless', 'Headless', 'HeadLegArmless']
for dname in dnames:
    print('###################################################')
    print('Generating:', dname)
    data = read_depth_images(dname)
    save_dataset(dname, data)
