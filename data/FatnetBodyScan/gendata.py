import csv
import numpy as np
import glob
import os
from PIL import Image
import scipy.io as sio

np.random.seed(23092017)

target_shape = (224,224)
nb_channels = 3

with open('raw_data/ground_truth.csv', 'r') as fp:
    ground_truth = np.asarray([row for row in csv.reader(fp) if '' not in row])

gt_labels = ground_truth[:, 0].astype(np.str) # subject label
gt_values = ground_truth[:, 6].astype(np.float32) # WBTOT_PFAT

def read_depth_images(dname):
    
    fnames = glob.glob('raw_data/' + dname + '/**/*.png', recursive=True)
    cnames = [os.path.basename(os.path.dirname(x)) for x in fnames]

    nb_samples = len(fnames)
    target_shape = (224, 224)

    data = {}
    imshape = target_shape + (nb_channels,)

    images = np.zeros((nb_samples,) + imshape, dtype=np.uint8)
    labels = np.array([''] * nb_samples, dtype='S32')
    values = np.zeros((nb_samples,), dtype=np.float32)
    train_idx = []
    test_idx = []

    nb_samples = len(fnames)
    count = 0
    new_cname = []
    for c in cnames:
        if c not in gt_labels:
            print('Skipping', c, 'because of missing ground-truth value')
            continue  # avoid classes without a value
        new_cname.append(c)
        idx = np.squeeze(np.argwhere(gt_labels == c))
        value = gt_values[idx]

        files = [x for x in fnames if x.find('/' + c + '/') != -1]
        for f in files:
            try:
                # load and resize the image
                image = Image.open(f)
                if image.size != target_shape:
                    image = image.resize(target_shape, Image.NEAREST)
                image = np.stack(nb_channels * [image])
                image = np.transpose(image, (1, 2, 0)) # HWC
                images[count] = image
                labels[count] = c
                values[count] = value
                count += 1
            except Exception as e:
                print('Skipping', f, 'because', e)
    
    images = images[:count]
    labels = np.array([l.decode('utf-8') for l in labels[:count]])
    values = values[:count]
    nb_samples = count

    idx = np.argsort(values)
    images = images[idx]
    labels = labels[idx]
    values = values[idx]

    # use friday-split :)
    import pickle
    with open('best_split_if_sorted.pkl', 'rb') as fp:
        data = pickle.load(fp)
        train_idx, test_idx = data
        train_idx = [i for i,x in enumerate(train_idx) if x is not None]
        test_idx = [i for i,x in enumerate(test_idx) if x is not None]

    # uniform split between train and test sets
    '''for i in range(0, nb_samples):
        if i % 5 > 0:
            train_idx.append(i)

    for i in range(0, nb_samples, 5):
        test_idx.append(i)'''

    print('Loaded', nb_samples, 'depth images')
    
    '''
    def remove_digits(string):
        tmp = ''.join([c for c in string if not c.isdigit()])
        return tmp

    sports = np.sort(list(set([remove_digits(l) for l in labels])))
    sport_labels = np.asarray([remove_digits(c) for c in labels])

    for s in sports:
        idx = np.squeeze(np.where(np.array(sport_labels) == s))
        athlets = np.sort(list(set(labels[idx])))
        for cid in range(0, athlets.size):
            if cid % 5 > 0:
                i = np.squeeze(np.where(labels == athlets[cid]))
                train_idx += [i] if i.size == 1 else i.tolist()
        for cid in range(0, athlets.size, 5):
            i = np.squeeze(np.where(labels == athlets[cid]))
            test_idx += [i] if i.size == 1 else i.tolist()
    '''

    train_idx = np.array(train_idx)
    test_idx = np.array(test_idx)

    return images, labels, values, train_idx, test_idx

def save_dataset(fname, data):
    print('Saving', fname, 'as mat file...')
    variables = {}
    variables['images'] = data[0]
    variables['labels'] = data[1]
    variables['values'] = data[2]
    variables['train_idx'] = data[3]
    variables['test_idx'] = data[4]

    dest_folder = './bin_data/'
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)
    sio.savemat(dest_folder + fname + '.mat', variables, do_compression=True)

# all
dnames = ['HeadLegArmlessB', 'HeadLegArmlessF', 'HeadlessB', 'HeadlessF', 'OriginalB', 'OriginalF', 'FullB', 'FullF', 'HeadLeglessB', 'HeadLeglessF']

# not analyzed for the paper
dnames = ['HeadLegArmlessB', 'HeadLegArmlessF', 'HeadlessB', 'HeadlessF', 'OriginalB', 'OriginalF']

# paper
#dnames = ['FullB', 'FullF', 'HeadLeglessB', 'HeadLeglessF'] <== PAPER

for dname in dnames:
    print('###################################################')
    print('Generating:', dname)
    data = read_depth_images(dname)
    save_dataset(dname, data)
