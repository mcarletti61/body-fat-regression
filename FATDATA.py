import torch
import scipy.io as sio
from PIL import Image
import numpy as np
import os

class FATDATA(torch.utils.data.Dataset):
    '''FatNet Dataset.

    Args:
        name (string): Dataset name [FullR, HeadLeglessR].
        train (bool, optional): If True, creates dataset from training set, otherwise
            creates from test set.
        transform (callable, optional): A function/transform that takes in an PIL image
            and returns a transformed version.
    '''

    def __init__(self, name, train=True, transform=None, nb_folds=None, fold_offset=None):
        parent = './data/FatnetBodyScan/bin_data/'
        if name in ['HeadLegArmlessB', 'HeadLegArmlessF', 'HeadlessB', 'HeadlessF', 'OriginalB', 'OriginalF', 'FullB', 'FullF', 'HeadLeglessB', 'HeadLeglessF']:
            self.matfile = parent + name + '.mat'
        else:
            raise Exception('Unknown dataset:', name)
        
        if not os.path.exists(self.matfile):
            raise Exception('Unable to find ' + name + '\nRun \'gendata.py\' from \'data\' folder.')
        
        self.transform = transform
        self.train = train  # training set or test set

        data = sio.loadmat(self.matfile)
        images = np.squeeze(data['images'])
        self.labels = np.squeeze(data['labels'])
        self.values = np.squeeze(data['values'])

        if nb_folds is None or nb_folds == 1:
            self.train_idx = np.squeeze(data['train_idx'])
            self.test_idx = np.squeeze(data['test_idx'])
        else:
            assert isinstance(nb_folds, int) and nb_folds >= 2
            assert isinstance(fold_offset, int) and 0 <= fold_offset < nb_folds
            self.train_idx, self.test_idx = [], []
            for i in range(self.values.size):
                if i % nb_folds == fold_offset:
                    self.test_idx.append(i)
                    continue
                self.train_idx.append(i)
            self.train_idx = np.asarray(self.train_idx)
            self.test_idx = np.asarray(self.test_idx)

        if self.train:
            self.train_data = images[self.train_idx]
            self.train_labels = self.labels[self.train_idx]
            self.train_values = self.values[self.train_idx]
        else:
            self.test_data = images[self.test_idx]
            self.test_labels = self.labels[self.test_idx]
            self.test_values = self.values[self.test_idx]

    def __getitem__(self, index):
        '''
        Args:
            index (int): Index.

        Returns:
            tuple: (image, target).
        '''
        if self.train:
            image, target, label = self.train_data[index], self.train_values[index], self.train_labels[index]
        else:
            image, target, label = self.test_data[index], self.test_values[index], self.test_labels[index]

        # doing this so that it is consistent with all other datasets
        # to return a PIL Image
        #image = np.transpose(image, (1, 2, 0))
        image = Image.fromarray(image)

        if self.transform is not None:
            image = self.transform(image)

        return image, target #, str(label)
    
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)

class REALDATA(torch.utils.data.Dataset):
    '''FatNet Dataset - Real data, acquired via Kinect.

    Args:
        name (string): Dataset name [Full, HeadLegless].
        transform (callable, optional): A function/transform that takes in an PIL image
            and returns a transformed version.
    '''

    def __init__(self, name, transform=None):
        parent = './data/FatnetKinect/bin_data/'
        if name in ['HeadLegArmless', 'Headless', 'Full', 'HeadLegless']:
            self.matfile = parent + name + '.mat'
        else:
            raise Exception('Unknown dataset:', name)
        
        assert os.path.exists(self.matfile)
        
        self.transform = transform

        data = sio.loadmat(self.matfile)
        self.images = np.squeeze(data['images'])
        self.values = np.squeeze(data['values'])

    def __getitem__(self, index):
        '''
        Args:
            index (int): Index.

        Returns:
            tuple: (image, target).
        '''
        image, target = self.images[index], self.values[index]

        image = Image.fromarray(image)
        if self.transform is not None:
            image = self.transform(image)

        return image, target
    
    def __len__(self):
        return len(self.values)