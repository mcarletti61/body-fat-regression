# -*- coding: utf-8 -*-

import argparse

import torch
from torch.autograd import Variable
import torchvision.transforms as transforms

import matplotlib.pyplot as plt
import numpy as np
import os

from utils import *
from metrics import mean_absolute_error
from FATDATA import FATDATA

# Results computation and visualization -------

def test_image(model, image, use_cuda=False, gpu_id=0):
    '''Test one single image, which must be a PyTorch FloatTensor (CxHxW).
    '''
    model.eval()

    if use_cuda:
        image = image.cuda(gpu_id)

    pred = model(Variable(image.unsqueeze(0)))
    pred = np.mean(np.squeeze(torch_to_numpy(pred)))

    return pred

def test(model, dataloader, loss_fun=None, score_fun=None, use_cuda=False, gpu_id=0, hflip=False):
    '''Test the dataloader, one image at time. If \'loss_fun\' and/ora \'score_fun\' are specified,
    also loss and score values are computed. If \'hflip\' is True, the prediction value of the
    i-th image is computed as the mean of the original image and its horizontal flipped
    version.
    '''
    
    model.eval()
    nb_samples = len(dataloader.dataset)
    predictions = []
    targets = []

    for i in range(nb_samples):
        image, target = dataloader.dataset[i]
        image.unsqueeze_(0)

        if hflip:
            flipped_image = image.numpy()[:,:,:,::-1].copy()
            image = torch.cat((image, torch.from_numpy(flipped_image)))

        if use_cuda:
            image = image.cuda(gpu_id)
        pred = model(Variable(image))
        pred = np.mean(np.squeeze(torch_to_numpy(pred)))
        
        predictions.append(pred)
        targets.append(target)

    predictions = np.asarray(predictions)
    targets = np.asarray(targets)

    loss, score = None, None

    if loss_fun is not None:
        p = numpy_to_torch(predictions, use_cuda, enable_grads=False)
        t = numpy_to_torch(targets, use_cuda, enable_grads=False)
        loss = torch_to_numpy(loss_fun(p, t))

    if score_fun is not None:
        score = score_fun(predictions, targets)

    return predictions, targets, loss, score

def show_outliers(arr, title='Outliers', show_immediately=False):
    '''Extract and show the outliers in the input vector.
    '''
    N = arr.size
    xx = np.linspace(0, N - 1, N)
    out_idx = find_outliers(arr)

    plt.figure()
    plt.plot(xx, arr, color='r', label='data')
    plt.scatter(out_idx, arr[out_idx], color='b', label='outliers')
    plt.title(title)
    plt.grid(True)
    plt.legend()

    if show_immediately:
        plt.show()

if __name__ == '__main__':

    seed = 23092017
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--model_dir', type=str, required=True,
                        help='results folder path')
    parser.add_argument('--input_shape', type=int, default=224,
                        help='input image resolution')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='number of samples per batch')
    parser.add_argument('--gpu_id', type=int, default=None,
                        help='enable cuda and use the specified gpu')
    parser.add_argument('--use_cuda', action='store_true',
                        help='enable cuda; if enabled and \'gpu_id\' is not set, the first available gpu will be used by default')
    parser.add_argument('--verbose', action='store_true',
                        help='print additional information')

    args = parser.parse_args()

    assert os.path.isdir(args.model_dir)

    if args.gpu_id is not None:
        args.use_cuda = True
    
    if args.use_cuda:
        if args.gpu_id is None:
            args.gpu_id = 0

    if args.verbose:
        print(args)

    #%% Load model -----------------------------------------------------------------

    # Normalize RGB-8 images between -1 and +1.
    transf = transforms.Compose([transforms.Scale((args.input_shape,args.input_shape)),
                                transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    model_name = os.path.join(args.model_dir, 'model.pth')
    # we assume the name of the model folder follows the format in the train.py script
    # that is, the first part of the folder name is the name of the FATDATA dataset
    b = os.path.basename(args.model_dir[:-1]) if args.model_dir[-1] == '/' else os.path.basename(args.model_dir)
    dsname = '_'.join(b.split('_')[0:1])

    train_predictions = []
    train_targets = []
    test_predictions = []
    test_targets = []

    if args.verbose:
        print('Loading dataset:', dsname)
    trainset = FATDATA(name=dsname, train=True, transform=transf)
    testset = FATDATA(name=dsname, train=False, transform=transf)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=False, num_workers=1)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=1)  

    if args.verbose:
        print('Loading model:', model_name)
    model = torch.load(model_name)
    model.eval()

    if args.use_cuda:
        model.cuda(args.gpu_id)

    # Testing -------------------------

    try:
        if args.verbose:
            print('Testing...')
        train_predictions, train_targets, _, _ = test(model, trainloader)
        test_predictions, test_targets, _, _ = test(model, testloader)
    except Exception as e:
        print(e.with_traceback, e.args)

    #%% Compute final scores with outliers -----------------------------------------

    print('#' * 60)
    print('Result considering outliers')

    train_mae = mean_absolute_error(train_predictions, train_targets)
    print('Train MAE:', train_mae)

    test_mae = mean_absolute_error(test_predictions, test_targets)
    print('Test MAE:', test_mae)

    '''
    #%% Compute outliers -----------------------------------------------------------

    train_err = train_predictions - train_targets
    test_err = test_predictions - test_targets

    train_err = train_predictions - train_targets
    test_err = test_predictions - test_targets

    train_out_idx = find_outliers(train_err)
    test_out_idx = find_outliers(test_err)

    #show_outliers(train_err, 'Train error')
    #show_outliers(test_err, 'Test error')
    #plt.show()

    #%% Compute final scores without outliers --------------------------------------

    print('#' * 60)
    print('Result without outliers')

    train_valid_idx = np.array([True] * train_targets.size)
    train_valid_idx[train_out_idx] = False
    train_mae = mean_absolute_error(train_predictions[train_valid_idx], train_targets[train_valid_idx])
    print('Train MAE:', train_mae)

    test_valid_idx = np.array([True] * test_targets.size)
    test_valid_idx[test_out_idx] = False
    test_mae = mean_absolute_error(test_predictions[test_valid_idx], test_targets[test_valid_idx])
    print('Test MAE:', test_mae)

    #%% Save outlier idx -----------------------------------------------------------

    fpath = os.path.join(args.model_dir, 'outliers.txt')
    with open(fpath, 'w') as fp:
        fp.write('train_out_idx\n')
        for id in train_out_idx:
            fp.write(str(id) + '\n')
        fp.write('test_out_idx\n')
        for id in test_out_idx:
            fp.write(str(id) + '\n')

    idx = np.argsort(train_targets)
    xx = np.linspace(0, train_targets.size - 1, train_targets.size)
    yy = np.where(train_valid_idx[idx] == False)[0]

    plt.figure()
    plt.plot(xx, train_targets[idx], color='g', label='target')
    plt.scatter(yy, train_targets[idx][yy], color='b', label='outliers')
    plt.title('Train outliers')
    plt.grid(True)
    plt.legend()
    plt.show()

    idx = np.argsort(test_targets)
    xx = np.linspace(0, test_targets.size - 1, test_targets.size)
    yy = np.where(test_valid_idx[idx] == False)[0]

    plt.figure()
    plt.plot(xx, test_targets[idx], color='g', label='target')
    plt.scatter(yy, test_targets[idx][yy], color='b', label='outliers')
    plt.title('Test outliers')
    plt.grid(True)
    plt.legend()
    plt.show()
    '''
