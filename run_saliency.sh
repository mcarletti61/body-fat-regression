#!/bin/bash

function show_help {
    echo "---"
    echo "Usage:"
    echo "      ./run_saliency.sh DATASET [GPU_ID]"
    echo ""
    echo "Arguments:"
    echo "  -h or --help    Prints this help"
    echo "  DATASET         must be 'HeadLegless', 'Full', 'HeadLegArmless', 'Headless', 'Original'; both Front and Back version will be computed"
    echo "  GPU_ID          [optional; default value: 0] positive integer defining the GPU to use"
    echo ""
    echo ""
    echo "The parameters following the second one will be ignored."
    echo "---"
}

if [ $# -eq 0 ]; then
    echo "Error: No arguments supplied"
    show_help
    exit -1
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    show_help
    exit 0
fi

if [ "$1" != "HeadLegless" ] && [ "$1" != "Full" ] && [ "$1" != "HeadLegArmless" ] && [ "$1" != "Headless" ] && [ "$1" != "Original" ]; then
    echo "Error: Invalid DATASET value"
    show_help
    exit -2
fi

if [ $# -eq 1 ]; then
    GPU_ID=""
elif [ "$2" -lt 0 ]; then
    echo "Error: Invalid GPU_ID value"
    show_help
    exit -3
else
    GPU_ID="--gpu_id $2"
fi

test_args="--cmap jet --mask_mode avg --k_size 23 --use_cuda $GPU_ID --verbose"

echo "--- SALIENCY ON TEST DATA ---"
python3 saliency.py --model_dir results/$1F* $test_args
python3 saliency.py --model_dir results/$1B* $test_args
