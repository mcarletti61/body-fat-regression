# -*- coding: utf-8 -*-

import argparse

import torch
from torch.autograd import Variable
import torchvision.transforms as transforms

import pickle
from scipy.misc import imresize
from scipy.ndimage import zoom
import matplotlib.pyplot as plt
import numpy as np
import sys
import os

from test import test_image
from FATDATA import FATDATA

def mask_image(image, u, v, k_size, mode='val', mask_value=0.):
    '''Creates a copy of the input image and change the values of the k_size*k_size window
    centered in (u,v) with the average value of the window in the original image if mode is \'avg\';
    otherwise replaces the pixels with \'mask_value\' if mode is \'val\'.
    '''
    assert mode in ['val', 'avg']
    h_size = int(k_size / 2)
    if mode == 'avg':
        mask_value = torch.mean(image[:, u-h_size:u+h_size, v-h_size:v+h_size])
    masked_image = image.clone()
    masked_image[:, u-h_size:u+h_size, v-h_size:v+h_size] = mask_value
    return masked_image

if __name__ == '__main__':

    seed = 23092017
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()

    parser.add_argument('--model_dir', type=str, required=True,
                        help='results folder path')
    parser.add_argument('--output_dir', type=str, default='saliency',
                        help='output directory where to save saliency results')
    parser.add_argument('--input_shape', type=int, default=224,
                        help='input image resolution')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='number of samples per batch')
    parser.add_argument('--k_size', type=int, default=23,
                        help='dimension of the mask kernel')
    parser.add_argument('--mask_mode', type=str, default='avg',
                        help='masking mode [val, avg]')
    parser.add_argument('--mask_value', type=int, default=0.,
                        help='masking value; ignored if mask_mode is \'avg\'')
    parser.add_argument('--cmap', type=str, default='jet',
                        help='colormap used to plot the images')
    parser.add_argument('--gpu_id', type=int, default=None,
                        help='enable cuda and use the specified gpu')
    parser.add_argument('--use_cuda', action='store_true',
                        help='enable cuda; if enabled and \'gpu_id\' is not set, the first available gpu will be used by default')
    parser.add_argument('--verbose', action='store_true',
                        help='print additional information')

    args = parser.parse_args()

    assert os.path.isdir(args.model_dir)
    assert args.mask_mode in ['avg', 'val']
    assert args.k_size >= 3 and args.k_size % 2 == 1

    if args.gpu_id is not None:
        args.use_cuda = True

    if args.use_cuda:
        if args.gpu_id is None:
            args.gpu_id = 0

    if args.verbose:
        print(args)

    target_shape = (args.input_shape, args.input_shape)

    #%% Load model -----------------------------------------------------------------

    # Normalize RGB-8 images between -1 and +1.
    transf = transforms.Compose([transforms.Resize(target_shape),
                                transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    model_name = os.path.join(args.model_dir, 'model.pth')
    # we assume the name of the model folder follows the format in the train.py script
    # that is, the first part of the folder name is the name of the FATDATA dataset
    b = os.path.basename(args.model_dir[:-1]) if args.model_dir[-1] == '/' else os.path.basename(args.model_dir)
    dsname = b.split('_')[0]

    if args.verbose:
        print('Loading dataset:', dsname)
    dataset = FATDATA(name=dsname, train=True, transform=transf, nb_folds=10, fold_offset=0)
    nb_samples = len(dataset)

    if args.verbose:
        print('Loading model:', model_name)
    model = torch.load(model_name)
    model.eval()

    if args.use_cuda:
        model.cuda(args.gpu_id)

    # Testing -------------------------

    filename = dsname + '_smaps.pickle'

    if os.path.exists(os.path.join(args.output_dir, filename)):
        with open(os.path.join(args.output_dir, filename), 'rb') as fp:
            smaps = pickle.load(fp)
    else:
        try:

            smaps = np.zeros((nb_samples,) + target_shape, dtype=np.float32)
            progress = ''

            if args.verbose:
                print('Evaluating saliency...')

            for i in range(nb_samples):
                if args.verbose:
                    progress = '\b' * len(progress) + 'Progress > {:3d}'.format(int(100 * (i + 1) / nb_samples))
                    print(progress, end='')
                    sys.stdout.flush()

                image, target_true = dataset[i]
                target_pred = test_image(model, image, args.use_cuda, gpu_id=args.gpu_id)

                target = target_pred

                saliency = []
                ch, rows, cols = image.shape
                h_size = int(args.k_size / 2)

                for u in range(h_size, rows, h_size):
                    for v in range(h_size, cols, h_size):
                        masked_image = mask_image(image, u, v, args.k_size, args.mask_mode)
                        masked_pred = test_image(model, masked_image, args.use_cuda, gpu_id=args.gpu_id)
                        pred_err = masked_pred - target
                        pred_err = pred_err if np.abs(pred_err) >= 0.01 else 0.0
                        saliency.append(pred_err)

                size = int(np.sqrt(len(saliency)))
                smap = np.array(saliency, dtype=np.float32)
                smap = smap.reshape((size, size))

                smaps[i] = zoom(smap, args.input_shape / size, order=1) # bilinear interpolation

            print('')

            if not os.path.exists(args.output_dir):
                os.makedirs(args.output_dir)

            with open(os.path.join(args.output_dir, filename), 'wb') as fp:
                pickle.dump(smaps, fp, pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            print(e.with_traceback, e.args)
            quit(-1)

    # change the first and second pixels only to scale the visualization
    '''
    m = np.maximum(np.abs(np.min(smaps)),np.abs(np.max(smaps)))
    smaps[:,0,0], smaps[:,0,1] = -m, m

    plt.figure()
    for i in range(smaps.shape[0]):
        plt.clf()
        plt.title('Saliency ' + str(dataset.values[i]))
        plt.imshow(smaps[i], cmap=plt.get_cmap(args.cmap))
        plt.colorbar()
        plt.axis('off')
        plt.pause(0.5)
    plt.show()
    '''

    #%% ---------------------------------------------------------------------------
    # Compute bins.

    bin_thr = 2.125 # mean accuracy between FullF and HeadLeglessF datasets
    bins = []
    current_bin = []
    lower_bound = dataset.values[0]

    for i in range(nb_samples):
        err = dataset.values[i] - lower_bound
        if err <= bin_thr:
            current_bin.append(i)
        else:
            bins.append(current_bin)
            lower_bound = dataset.values[i]
            current_bin = [i]
    bins.append(current_bin)

    nb_bins = len(bins)

    '''
    plt.figure()
    plt.title('Datset split')
    for b in bins:
        plt.plot(np.linspace(b[0], b[-1], len(b)), dataset.values[b])
    plt.grid(True)
    plt.show()
    '''

    #%% ---------------------------------------------------------------------------
    # Show bin means.

    bin_means = np.zeros((nb_bins,) + target_shape, dtype=np.float32)
    for i, b in enumerate(bins):
        bmean = np.zeros(target_shape, dtype=np.float32)
        for m in smaps[b]:
            bmean += m
        bin_means[i] = bmean / len(b)

    filename = dsname + '_groups.pickle'
    with open(os.path.join(args.output_dir, filename), 'wb') as fp:
        pickle.dump(bin_means, fp, pickle.HIGHEST_PROTOCOL)

    # change the first and second pixels only to scale the visualization
    bin_means = bin_means - np.min(bin_means)
    bin_means = bin_means / np.max(bin_means)
    M = np.max(np.abs(bin_means))
    bin_means[:,0,0], bin_means[:,0,1] = -1, 1

    # show bin-saliency "video"
    nb_bins += 1
    nrows = int(round(np.sqrt(nb_bins)))
    ncols = int(round(nb_bins / nrows + 0.5))
    title = 'Mean saliency maps'
    hf = plt.figure(figsize=(ncols * 6,nrows * 6))
    plt.suptitle(title)
    for i in range(nb_bins - 1):
        plt.subplot(nrows, ncols, i + 1)
        plt.imshow(bin_means[i], cmap=plt.get_cmap(args.cmap))
        plt.axis('off')

    #plt.subplot(nrows, ncols, i + 2)
    #plt.imshow(np.zeros(bin_means[i].shape), cmap=plt.get_cmap(args.cmap))
    #plt.colorbar()

    plt.savefig(os.path.join(args.output_dir, dsname + '.png'))
    #plt.show()
