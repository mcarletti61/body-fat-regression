# -*- coding: utf-8 -*-

import torch
from torch.autograd import Variable
import torchvision.transforms as transforms

import os
import scipy.io as sio
import numpy as np
import csv

# Conversion ----------------------------------

def numpy_to_torch(array, use_cuda=False, gpu_id=0, enable_grads=True, as_batch=False, to_float32=True):
    L = len(array.shape)
    # convert vectors to float32 arrays
    # when the array has one dimension, we do not
    # add channel information
    if L == 1:
        array = np.float32(array) if to_float32 else array
    # add channels information to 2D arrays
    # any torch tensor will have the first dimension
    # corresponding to the number of channels
    elif L == 2:
        array = np.array([array])
    # we assume that the internal format of the array
    # is HWC, so we need to move the channels in the
    # first position
    elif L == 3:
        array = np.transpose(array, (2,0,1))
    else:
        raise ValueError('Input array must be 1, 2 or 3d')
    # convert to float32 if asked
    array = np.float32(array) if to_float32 else array
    # avoid non negative strides: data is stored orderly
    # BUG: PyTorch does not yet support negative strides
    array = np.ascontiguousarray(array)
    tensor = torch.from_numpy(array)
    # we expand and/or move to gpu the tensor data
    # BEFORE converting it to a Variable so it can be
    # used as leaf-tensor in the optimization graph
    if as_batch:
        tensor.unsqueeze_(0)
    if use_cuda:
        tensor = tensor.cuda(gpu_id)
    # create variable with/without gradients
    tensor = Variable(tensor, requires_grad=enable_grads)
    return tensor

def torch_to_numpy(tensor):
    # make the tensor a variable to be able to
    # access its data without throwing exceptions
    if not isinstance(tensor, Variable):
        tensor = Variable(tensor)
    # move to cpu and convert the data to a numpy array 
    array = tensor.cpu().data.numpy()
    # remove useless dimensions
    array = np.squeeze(array)
    # return a scalar if the array has one single value
    array = np.asscalar(array) if array.size == 1 else array
    return array

# PyTorch -------------------------------------

def get_transf(input_shape=(224,224)):
    mu = (0.485, 0.456, 0.406)
    sd = (0.229, 0.224, 0.225)

    # Normalize RGB-8 images between -1 and +1.
    transf = transforms.Compose([transforms.Resize((input_shape,input_shape)),
                                transforms.ToTensor(),
                                transforms.Normalize(mu, sd)])
    return transf

def enable_grads(model, enable=True):
    for param in model.parameters():
        param.requires_grad = enable

def normal_init(module, mu=0., std=0.01):
    module.weight.data.normal_(mu, std)
    module.bias.data.fill_(mu)

# Save and load -------------------------------

def savemat(filename, data):
    try:
        sio.savemat(filename, data, do_compression=True)
    except Exception as e:
        raise e

def loadmat(filename):
    try:
        data = sio.loadmat(filename)
        return data
    except Exception as e:
        raise e

# Misc ----------------------------------------

def find_outliers(arr):
    mu = np.mean(arr)
    sd = np.std(arr)
    idx1 = np.where(arr < mu - 2 * sd)[0]
    idx2 = np.where(arr > mu + 2 * sd)[0]
    return np.sort(np.concatenate([idx1, idx2]))

def softmax(arr):
    e = np.exp(arr)
    s = e / np.sum(e)
    return s, np.argmax(s)
    
def load_valid_classes(filename):
	valid_classes = []
	with open(filename, 'r') as fp:
		stream = csv.reader(fp)
		for x in stream:
			valid_classes.append(x[0])
	return sorted(valid_classes)

def mkdir(path):
	'''Create a folder if it does not exist.
	'''
	if not os.path.exists(path):
		os.makedirs(path)

def normalize(array):
    array -= np.min(array)
    array /= np.max(array)
    return array

def load_image(filename, imsize=(224,224), divide_by=10000.):
    from PIL import Image
    import numpy as np
    raw_data = np.asarray(Image.open(filename).resize(imsize).convert('F'), dtype=np.float32) / divide_by
    u, v = np.where(raw_data == 0)
    cam_dist = 0.6
    offset = 0.2
    imdata = (raw_data - (cam_dist - offset))
    imdata = imdata / (2 * offset)
    imdata[u, v] = 0.
    return np.uint8(255 * imdata)