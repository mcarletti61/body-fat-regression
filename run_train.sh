#!/bin/bash

function show_help {
    echo "---"
    echo "Usage:"
    echo "      ./run_train.sh DATASET [GPU_ID]"
    echo ""
    echo "Arguments:"
    echo "  -h or --help    Prints this help"
    echo "  DATASET         must be 'HeadLegless', 'Full', 'HeadLegArmless', 'Headless', 'Original'; both Front and Back version will be computed"
    echo "  GPU_ID          [optional; default value: 0] positive integer defining the GPU to use"
    echo ""
    echo ""
    echo "The parameters following the second one will be ignored."
    echo "---"
}

if [ $# -eq 0 ]; then
    echo "Error: No arguments supplied"
    show_help
    exit -1
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    show_help
    exit 0
fi

if [ "$1" != "HeadLegless" ] && [ "$1" != "Full" ] && [ "$1" != "HeadLegArmless" ] && [ "$1" != "Headless" ] && [ "$1" != "Original" ]; then
    echo "Error: Invalid DATASET value"
    show_help
    exit -2
fi

if [ $# -eq 1 ]; then
    GPU_ID=""
elif [ "$2" -lt 0 ]; then
    echo "Error: Invalid GPU_ID value"
    show_help
    exit -3
else
    GPU_ID="--gpu_id $2"
fi

train_args="--batch_size 16 --epochs 40 --learning_rate 0.0001 --augment_all --verbose --use_cuda $GPU_ID"
plot_images_args="--history --predictions --bland_altman --train --test --sort --save_figures"

echo "--- TRAIN FRONT VIEW ---"
python3 train.py --dataset $1F $train_args
python3 plot_images.py --model_dir results/$1F* $plot_images_args

echo "--- TRAIN BACK VIEW ---"
python3 train.py --dataset $1B $train_args
python3 plot_images.py --model_dir results/$1B* $plot_images_args
