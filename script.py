#!/bin/env python3

import argparse
from joblib import Parallel, delayed
import time

def parallel_func(args, gpu_id=None):
    print(gpu_id, args)
    time.sleep(1)

def run(args):
    if args.use_cuda:
        try:
            GPUS = [0, 1]
            _ = Parallel(n_jobs=len(GPUS), backend='threading')(delayed(parallel_func)(args, gid) for gid in GPUS)
        except Exception as e:
            print(e)
            quit()
    else:
        parallel_func(args, None)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--modelname', type=str, required=True, help='Pretrained deep architecture')
    parser.add_argument('--use_cuda', action='store_true', help='Enable cuda usage')
    parser.add_argument('--verbose', action='store_true', help='Enable verbose mode')
    args = parser.parse_args()

    run(args)
