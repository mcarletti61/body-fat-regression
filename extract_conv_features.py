# -*- coding: utf-8 -*-

import argparse

import torch
from torch.autograd import Variable
from torchvision.models import alexnet, inception_v3, vgg16, resnet50
import torchvision.transforms as transforms

import time
import pickle
import matplotlib.pyplot as plt
import numpy as np
import sys
import os

from utils import *
from FATDATA import FATDATA

def run(args):

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    models = {'alexnet': alexnet,   'googlenet': inception_v3, 'vgg': vgg16,     'resnet': resnet50}
    shapes = {'alexnet': (224,224), 'googlenet': (299,299),    'vgg': (224,224), 'resnet': (224,224)}

    if args.verbose:
        print(args)

    assert args.model in models.keys()

    #%% Load data ------------------------------------------------------------------

    try:

        # Normalize RGB-8 images between -1 and +1.
        transf = transforms.Compose([transforms.Resize(shapes[args.model]),
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

        trainset = FATDATA(name=args.dataset, train=True, transform=transf)
        testset = FATDATA(name=args.dataset, train=False, transform=transf)

        trainloader = torch.utils.data.DataLoader(trainset, batch_size=1, shuffle=False, num_workers=1)
        testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False, num_workers=1)

    except Exception as e:
        #raise Exception('Unknown dataset name or path: ' + args.dataset)
        raise e

    #%% Load model -----------------------------------------------------------------

    # Set model.
    model = models[args.model](pretrained=True)
    model.eval()
    model.to(device)

    if args.model in ['alexnet', 'vgg']:
        layer = model.features[-1]
        ydata = model.features(torch.zeros(1,3,224,224))
        nfts = ydata.view(-1).numel()

    if args.model in ['googlenet', 'resnet']:
        layer = model._modules.get('avgpool')
        # the 'avgpool' layer has an output size of 512
        nfts = 2048
    
    def getConvFeatures(xdata):
            
        my_embedding = torch.zeros(nfts)

        # define a function that will copy the output of a layer
        def copy_data(m, i, o):
            my_embedding.copy_(o.view(-1).data)

        # attach that function to our selected layer
        h = layer.register_forward_hook(copy_data)
        # run the model on our transformed image
        model(xdata)
        # detach our copy function from the layer
        h.remove()

        # return the feature vector
        return my_embedding

    if args.VERBOSE:
        print(model)

    #%% Extractor ------------------------------------------------------------------

    def extract_features(dataloader):

        features = []

        if args.verbose:
            print('Extracting features...')

        for xdata, ydata, label in dataloader:
            xdata = xdata.to(device)
            fts = getConvFeatures(xdata)
            fts = fts.detach().cpu().numpy().tolist()
            features.append([label[0]] + fts)
        
        return np.asarray(features)
    
    trFeats = extract_features(trainloader)
    np.savetxt('data/trFeats.txt', trFeats, fmt='%s')

    tsFeats = extract_features(testloader)
    np.savetxt('data/tsFeats.txt', tsFeats, fmt='%s')

if __name__ == '__main__':

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()

    parser.add_argument('--model', type=str, default='resnet',
                        help='off-the-shelf pretrained model to fine-tune; must be in [\'alexnet\', \'vgg\', \'resnet\', \'googlenet\']')
    parser.add_argument('--dataset', type=str, default='HeadLeglessF',# required=True,
                        help='dataset name; must be in [\'HeadLegArmlessB\', \'HeadLegArmlessF\', \'HeadlessB\', \'HeadlessF\', \'OriginalB\', \'OriginalF\', \'FullB\', \'FullF\', \'HeadLeglessB\', \'HeadLeglessF\']')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--VERBOSE', action='store_true')

    args = parser.parse_args()

    run(args)
