import pickle
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.pyplot import imread
from scipy.misc import imsave
from scipy.ndimage import zoom
import os

colors = [(0.0, 0.0, 1.0),
          #(0.0, 1.0, 1.0),
          (0.0, 0.0, 0.0),
          #(1.0, 1.0, 0.0),
          (1.0, 0.0, 0.0)]  # B -> K -> R
cmap_name = 'saliency'
custom_cmap = LinearSegmentedColormap.from_list(cmap_name, colors, N=256)

def mean(data):
	m = np.zeros(data[0].shape, dtype=np.float32)
	for i in range(data.shape[0]):
		m += data[i]
	m /= data.shape[0]
	return m

def resize(mat, shape, order=3):
	zh = shape[0] / mat.shape[0]
	zw = shape[1] / mat.shape[1]
	return zoom(mat, [zh, zw], order)

def mat2gray(mat):
	im = mat.astype(np.float32)
	im = im - np.min(im)
	im = im / np.max(im)
	im = np.uint8(255. * im)
	return im

def gray2rgb(bw, cmap=plt.get_cmap('jet')):
	im = mat2gray(bw)
	rgb = np.delete(cmap(im), 3, 2)
	return rgb

import sys
#print(len(sys.argv), sys.argv)
if len(sys.argv) < 3:
	print('ERROR: Invalid syntax')
	print('Usage:\n\tpython3 tmp.py <dsname> <outdir>')
	quit(-1)
dsname = sys.argv[1]
outdir = sys.argv[2]

with open(dsname + '_groups.pickle', 'rb') as fp:
	groups = pickle.load(fp)

with open(dsname + '_smaps.pickle', 'rb') as fp:
	smaps = pickle.load(fp)

gm = mean(groups)
sm = mean(smaps)

if not os.path.exists(outdir):
	os.makedirs(outdir)

data = {}
data['smaps'] = smaps
import scipy.io as sio
sio.savemat(outdir + '/' + dsname + '.mat', data)

#%%

#im1 = imread('../data/FatnetBodyScan/raw_data/' + dsname + '/rugby26/img.png')
#im2 = imread('../data/FatnetBodyScan/raw_data/' + dsname + '/volley_new5/img.png')
#imsave('tmp/im1.png', mat2gray(im1))
#imsave('tmp/im2.png', mat2gray(im2))

#%%

#plt.imshow(sm, cmap=plt.get_cmap('seismic'))
#plt.colorbar()
#plt.show()

#imsave('tmp/sm.png', gray2rgb(mat2gray(sm), plt.get_cmap('seismic')))

#%%

quit(0)

def save_smap(i, cm=plt.get_cmap('seismic')):
	im = smaps[i].copy()
	im[0,0] = -np.max(np.abs(im))
	im[0,1] = +np.max(np.abs(im))
	#plt.figure()
	#plt.imshow(im, cmap=cm)
	#plt.colorbar()
	imsave(outdir + '/' + dsname + '/{:03d}'.format(i) + '.png', gray2rgb(im, cm))
	#plt.show()

if not os.path.exists(outdir + '/' + dsname):
	os.makedirs(outdir + '/' + dsname)

print('Saving images...')
for i in range(smaps.shape[0]):
	save_smap(i)

#plt.show()
