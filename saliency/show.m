close all;
clear;
clc;

%%

root = 'full_smaps';
N = 315;

figure(1);
axis equal;

for i = 1:N
    fname = [root '/' pad(num2str(i),3,'left','0') '.png'];
    img = imread(fname);
    imagesc(img);
    F(i) = getframe(gcf);
    drawnow;
end

%%

root = '/home/marco/Documents/body-fat-regression/data/results/FullF/FullF_2018:141_10:';
P = [];
R = [];

for i = 0:9
    load([root num2str(i) '/results.mat']);
    P = [P test_preds];
    R = [R test_targets];
end

[~, idx] = sort(R);
R = R(idx);
P = P(idx);

figure(2);

for i = 1:350
    plot(linspace(1,350,350),R,'g','LineWidth',2);
    hold on;
    grid on;
    plot(linspace(1,350,350),P,'b','LineWidth',2);
    line([i i], [0 max(R)],'LineWidth',3);
    hold off;
    F(i) = getframe(gcf);
    drawnow;
end

%%

% create the video writer with 1 fps
writerObj = VideoWriter(['line.avi']);
writerObj.FrameRate = 17;
% set the seconds per image
% open the video writer
open(writerObj);

% write the frames to the video
for i=2:length(F)
    % convert the image to a frame
    frame = F(i);
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);
