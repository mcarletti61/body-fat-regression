# -*- coding: utf-8 -*-

import argparse

import torch
from torch.autograd import Variable
from torchvision.models import alexnet, inception_v3, vgg16, resnet50
import torchvision.transforms as transforms

import time
import pickle
import matplotlib.pyplot as plt
import numpy as np
import sys
import os

from utils import *
from metrics import mean_absolute_error
from FATDATA import FATDATA

from test import test

#%% Optimization ------------------------------

def train(model, dataloader, optimizer, loss_fun, score_fun, verbose=False, args=None):
    '''Train the model using dataloader for one epoch, using batches.
    The evaluation of the training is done at the end of the epoch.
    Data augmentation is performed as hard examples mining, horizontal flip
    and gaussian noise augmentation.
    '''
    
    model.train()
    nb_batches = len(dataloader)
    progress = ''

    # train on the entire dataset once
    for batch_idx, (inputs, targets) in enumerate(dataloader):
        if verbose:
            progress = '\b' * len(progress) + 'Progress > {:3d}'.format(int(100 * (batch_idx + 1) / nb_batches))
            print(progress, end='')
            sys.stdout.flush()
        
        # Hard Examples Mining (data augmentation)
        if args.augment_mining:
            tmp = torch_to_numpy(Variable(targets))
            #hard_examples_idx = np.where(tmp.any() < args.augment_mining_min_bound or tmp.any() > args.augment_mining_max_bound)
            hard_examples_idx = np.where(np.logical_or(np.less(tmp, args.augment_mining_min_bound), np.greater(tmp, args.augment_mining_max_bound)))[0]
            if hard_examples_idx.size > 0:
                hard_examples_idx = torch.LongTensor(hard_examples_idx)
                inputs = torch.cat((inputs, inputs[hard_examples_idx]))
                targets = torch.cat((targets, targets[hard_examples_idx]))

        # hirizontal flip (data augmentation)
        if args.augment_hflip:
            flipped_images = inputs.numpy()[:,:,:,::-1].copy()
            inputs = torch.cat((inputs, torch.from_numpy(flipped_images)))
            targets = torch.cat((targets, targets))
        
        # add noise (data augmentation)
        if args.augment_noise:
            noise_scale = 0.05
            images = inputs.numpy().copy()
            noise = (np.random.rand(*images.shape) - 0.5) * noise_scale
            noisy_images = (images + noise).astype(np.float32)
            inputs = torch.cat((inputs, torch.from_numpy(noisy_images)))
            targets = torch.cat((targets, targets))

        # prepare inputs
        inputs, targets = Variable(inputs), Variable(targets)
        if args.use_cuda:
            inputs, targets = inputs.cuda(args.gpu_id), targets.cuda(args.gpu_id)

        # compute predictions
        outputs = model(inputs)
        loss = loss_fun(outputs, targets)

        # optimization step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    if verbose:
        print('')

    # compute epoch statistics
    _, _, epoch_loss, epoch_score = test(model, dataloader, loss_fun, score_fun, use_cuda=args.use_cuda, gpu_id=args.gpu_id)

    return epoch_loss, epoch_score

def run(args):

    seed = 23092017
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    models = {'alexnet': alexnet,   'googlenet': inception_v3, 'vgg': vgg16,     'resnet': resnet50}
    shapes = {'alexnet': (224,224), 'googlenet': (299,299),    'vgg': (224,224), 'resnet': (224,224)}

    if args.augment_all:
        args.augment_mining = True
        args.augment_hflip = True
        args.augment_noise = True

    if args.gpu_id is not None:
        args.use_cuda = True
    
    if args.use_cuda:
        if args.gpu_id is None:
            args.gpu_id = 0

    if args.verbose:
        print(args)

    assert args.model in models.keys()
    assert args.optimizer in ['sgd', 'adam', 'rms']

    #%% Load data ------------------------------------------------------------------

    try:
        # Normalize RGB-8 images between -1 and +1.
        transf = transforms.Compose([transforms.Resize(shapes[args.model]),
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

        trainset = FATDATA(name=args.dataset, train=True, transform=transf, nb_folds=args.nb_folds, fold_offset=args.fold_offset)
        testset = FATDATA(name=args.dataset, train=False, transform=transf, nb_folds=args.nb_folds, fold_offset=args.fold_offset)

        trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=1)
        testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=1)
    except Exception as e:
        #raise Exception('Unknown dataset name or path: ' + args.dataset)
        raise e

    #%% Load model -----------------------------------------------------------------

    # Set model.
    model = models[args.model](pretrained=True)
    model.train()

    def freeze_layers(model, enable=True):
        for param in model.parameters():
            model.requires_grad = enable

    def normal_init(module, mu=0., std=0.01):
        module.weight.data.normal_(mu, std)
        module.bias.data.fill_(mu)

    #freeze_layers(model, False)

    if args.model in ['alexnet', 'vgg']:
        linear_layer_id = 0
        while type(model.classifier[linear_layer_id]) is not torch.nn.Linear:
            linear_layer_id += 1
        nfts = model.classifier[linear_layer_id].in_features
        model.classifier = torch.nn.Linear(nfts, 1)
        normal_init(model.classifier)
        #regressor = model.classifier
    elif args.model in ['googlenet', 'resnet']:
        nfts = model.fc.in_features
        model.fc = torch.nn.Linear(nfts, 1)
        normal_init(model.fc)
        #regressor = model.fc
    else:
        raise Exception('Unknown model name:', args.model)

    loss_fun = torch.nn.SmoothL1Loss()
    score_fun = mean_absolute_error

    if args.use_cuda:
        model.cuda(args.gpu_id)
        loss_fun.cuda(args.gpu_id)

    if args.verbose:
        print(model)

    if args.optimizer == 'sgd':
        optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate, momentum=args.momentum, weight_decay=args.weight_decay)
    elif args.optimizer == 'adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)
    elif args.optimizer == 'rms':
        optimizer = torch.optim.RMSprop(model.parameters(), lr=args.learning_rate, momentum=args.momentum, weight_decay=args.weight_decay)
    else:
        raise Exception('Unknown optimizer:', args.optimizer)

    #%% Training -------------------------------------------------------------------

    def adjust_learning_rate(optimizer, epoch, step=1):
        """Sets the learning rate to the initial LR halved every 'step' epochs"""
        lr = args.learning_rate * (0.5 ** (epoch // step))
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr

    try:
        history = []
        best_test_score = float('Inf')
        
        print('Training...')

        nb_epochs = args.epochs
        for epoch in range(nb_epochs):
            print('#' * 60)
            print('Epoch [{:3d}|{:3d}]'.format(epoch+1, nb_epochs))

            adjust_learning_rate(optimizer, epoch, step=5)
            
            # train one epoch and evaluate the trainset at the end
            train_loss, train_score = train(model, trainloader, optimizer, loss_fun, score_fun, verbose=args.verbose, args=args)
            print('Train - Loss:', train_loss, 'Score:', train_score)

            # evaluate generalization
            _, _, test_loss, test_score = test(model, testloader, loss_fun, score_fun, use_cuda=args.use_cuda, gpu_id=args.gpu_id)
            print('Test -- Loss:', test_loss, 'Score:', test_score)
        
            # save partial results
            if args.save_ckpt and test_score < best_test_score:
                best_test_score = test_score
                ckpt_folder = os.path.join(args.output_dir, 'ckpt/')
                if not os.path.exists(ckpt_folder):
                    os.makedirs(ckpt_folder)
                os.remove(os.path.join(ckpt_folder, '*.ckpt'))
                torch.save(model, os.path.join(ckpt_folder, 'model_at_epoch_' + str(epoch + 1) + '.ckpt'))
            
            # update history
            history.append([[train_loss, train_score], [test_loss, test_score]])
        
        history = np.asarray(history)

        # create root folder for results
        if not os.path.exists(args.output_dir):
            os.makedirs(args.output_dir)
        
        # create final destination folder
        date = time.strptime(time.asctime())
        date = [date.tm_year, date.tm_yday, date.tm_hour, date.tm_min]
        date = [str(date[0]), '{:03d}'.format(date[1]), '{:02d}'.format(date[2]), '{:02d}'.format(date[3])]
        date = date[:2] # do not consider time, only year and day
        date = ':'.join([x for x in date])
        dst_folder = args.dataset + '_' + date + '_' + str(args.nb_folds) + ':' + str(args.fold_offset)
        dst_folder = os.path.join(args.output_dir, dst_folder)
        if not os.path.exists(dst_folder):
            os.makedirs(dst_folder)

        # save train info    
        with open(os.path.join(dst_folder, 'info.txt'), 'w') as fp:
            info = str(vars(args))[1:-1]
            info = info.split(', ')        
            info = '\n'.join(info)
            fp.write(info)

        # save final model
        # save cpu version to avoid unsupported gpu issues
        print('Saving model... ', end='')
        torch.save(model.cpu(), os.path.join(dst_folder, 'model.pth'))
        print('DONE')

        if args.use_cuda:
            model.cuda(args.gpu_id)

        # compute, print and save final results
        print('#' * 60)
        print('Final results:')
        results = {}
        results['history'] = history
        preds, targets, loss, score = test(model, trainloader, loss_fun, score_fun, use_cuda=args.use_cuda, gpu_id=args.gpu_id)
        print('Train - Loss:', loss, 'Score:', score)
        results['train_preds'] = preds
        results['train_targets'] = targets
        results['train_loss'] = loss
        results['train_score'] = score
        preds, targets, loss, score = test(model, testloader, loss_fun, score_fun, use_cuda=args.use_cuda, gpu_id=args.gpu_id)
        print('Test -- Loss:', loss, 'Score:', score)
        results['test_preds'] = preds
        results['test_targets'] = targets
        results['test_loss'] = loss
        results['test_score'] = score

        # save data idx to merge later
        results['train_idx'] = trainset.train_idx
        results['test_idx'] = testset.test_idx

        # save numerical results and predictions
        #with open(os.path.join(dst_folder, 'results.pickle'), 'wb') as fp:
        #    pickle.dump(results, fp, pickle.HIGHEST_PROTOCOL)
        print('Saving results... ', end='')
        savemat(os.path.join(dst_folder, 'results.mat'), results)
        with open(os.path.join(dst_folder, 'scores.txt'), 'w') as fp:
            info = 'Train \tLoss: {:.3f}'.format(results['train_loss']) + ' \tScore: {:.3f}'.format(results['train_score']) + '\n'
            fp.write(info)
            info = 'Test \tLoss: {:.3f}'.format(results['test_loss']) + ' \tScore: {:.3f}'.format(results['test_score']) + '\n'
            fp.write(info)
        print('DONE')

        print('#' * 60)
        print('DONE - Result files saved in:', dst_folder)
        print('#' * 60)
        print('#' * 60)
        
    except Exception as e:
        print(e.with_traceback, e.args)

if __name__ == '__main__':

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()

    parser.add_argument('--model', type=str, default='resnet',
                        help='off-the-shelf pretrained model to fine-tune; must be in [\'alexnet\', \'vgg\', \'resnet\', \'googlenet\']')
    parser.add_argument('--dataset', type=str, required=True,
                        help='dataset name; must be in [\'HeadLegArmlessB\', \'HeadLegArmlessF\', \'HeadlessB\', \'HeadlessF\', \'OriginalB\', \'OriginalF\', \'FullB\', \'FullF\', \'HeadLeglessB\', \'HeadLeglessF\']')
    parser.add_argument('--output_dir', type=str, default='results',
                        help='output directory where to save saliency results')
    parser.add_argument('--epochs', type=int, default=40,
                        help='total number of iteration on the entire training set')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='number of samples per batch')
    parser.add_argument('--optimizer', type=str, default='adam',
                        help='optimizer type [sgd, adam, rms]')
    parser.add_argument('--learning_rate', type=float, default=0.0001,
                        help='optimizer learning rate coefficient')
    parser.add_argument('--momentum', type=float, default=0.9,
                        help='optimizer momentum coefficient')
    parser.add_argument('--weight_decay', type=float, default=0.0005,
                        help='L2-regularizer coefficient')
    parser.add_argument('--augment_all', action='store_true',
                        help='data augmentation; enable all methods of data augmentation')
    parser.add_argument('--augment_mining', action='store_true',
                        help='data augmentation by mining hard examples - WARNING: this increase the batch size')
    parser.add_argument('--augment_mining_min_bound', type=float, default=10.,
                        help='if a sample target is lower than \'augment_mining_min_bound\', the sample is duplicated')
    parser.add_argument('--augment_mining_max_bound', type=float, default=18.,
                        help='if a sample target is higher than \'augment_mining_max_bound\', the sample is duplicated')
    parser.add_argument('--augment_hflip', action='store_true',
                        help='data augmentation by horizontal flip - WARNING: this doubles the batch size')
    parser.add_argument('--augment_noise', action='store_true',
                        help='data augmentation by adding gaussin noise - WARNING: this increase the batch size')
    parser.add_argument('--gpu_id', type=int, default=None,
                        help='enable cuda and use the specified gpu')
    parser.add_argument('--use_cuda', action='store_true',
                        help='enable cuda; if enabled and \'gpu_id\' is not set, the first available gpu will be used by default')
    parser.add_argument('--save_ckpt', action='store_true',
                        help='during training, save in \'ckpt\' folder the best state of the network according to testing results')
    parser.add_argument('--verbose', action='store_true',
                        help='print additional information')
    parser.add_argument('--nb_folds', type=int, default=None,
                        help='size of crossfolding')
    parser.add_argument('--fold_offset', type=int, default=None,
                        help='fold to consider (between 0 and nb_folds-1)')

    args = parser.parse_args()

    run(args)
