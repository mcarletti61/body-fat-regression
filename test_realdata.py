# -*- coding: utf-8 -*-

import argparse

import torch
from torch.autograd import Variable
import torchvision.transforms as transforms

import matplotlib.pyplot as plt
import numpy as np
import os

from utils import numpy_to_torch, torch_to_numpy
from metrics import mean_absolute_error
from FATDATA import REALDATA

from test import test
from plot_images import show_prediction, bland_altman_plot

if __name__ == '__main__':

    seed = 23092017
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    #%% Check arguments ------------------------------------------------------------

    # Parse arguments.
    parser = argparse.ArgumentParser()

    parser.add_argument('--model_dir', type=str, required=True,
                        help='results folder path')
    parser.add_argument('--dataset', type=str, required=True,
                        help='dataset name; must be in [\'HeadLegArmlessB\', \'HeadLegArmlessF\', \'HeadlessB\', \'HeadlessF\', \'OriginalB\', \'OriginalF\', \'FullB\', \'FullF\', \'HeadLeglessB\', \'HeadLeglessF\']')
    parser.add_argument('--input_shape', type=int, default=224,
                        help='input image resolution')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='number of samples per batch')
    parser.add_argument('--gpu_id', type=int, default=None,
                        help='enable cuda and use the specified gpu')
    parser.add_argument('--use_cuda', action='store_true',
                        help='enable cuda; if enabled and \'gpu_id\' is not set, the first available gpu will be used by default')
    parser.add_argument('--verbose', action='store_true',
                        help='print additional information')

    args = parser.parse_args()

    if args.gpu_id is not None:
        args.use_cuda = True
    
    if args.use_cuda:
        if args.gpu_id is None:
            args.gpu_id = 0

    if args.verbose:
        print(args)

    #%% Load model -----------------------------------------------------------------

    if args.verbose:
        print('Loading dataset:', args.dataset)

    # Normalize RGB-8 images between -1 and +1.
    transf = transforms.Compose([transforms.Resize((args.input_shape,args.input_shape)),
                                transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    dataset = REALDATA(name=args.dataset, transform=transf)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=1)  

    model_name = os.path.join(args.model_dir, 'model.pth')
    if args.verbose:
        print('Loading model:', model_name)
    model = torch.load(model_name)
    model.eval()

    if args.use_cuda:
        model.cuda(args.gpu_id)

    # Testing -------------------------

    try:
        if args.verbose:
            print('Evaluating...')

        predictions, targets, _, _ = test(model, dataloader, use_cuda=args.use_cuda, gpu_id=args.gpu_id, hflip=True)

        data = [predictions, targets]
        data = np.array(data).swapaxes(0,1)
        np.savetxt('real.txt', data, fmt='%s')

        if args.verbose:
            print('#' * 60)

        mae = mean_absolute_error(predictions, targets)
        print('Prediction MAE:', mae)

        if args.verbose:
            err = predictions - targets
            mu, var, sd = np.mean(err), np.var(err), np.std(err)
            print('Error Mean (Var) (Std): {:.3f} ({:.3f}) ({:.3f})'.format(mu, var, sd))

            show_prediction(predictions, targets, title='Predictions on real data', show_immediately=True)

    except Exception as e:
        print(e.with_traceback, e.args)
