import types
import multiprocessing as mp
from joblib import Parallel, delayed
import train
import script

def parallel_func(args):
    train.run(args)

def run(paramsets):
    try:
        N = len(paramsets)
        J = 1 #min(N, mp.cpu_count())
        _ = Parallel(n_jobs=J, backend='threading')(delayed(parallel_func)(paramsets[i]) for i in range(N))
    except Exception as e:
        print(e)

if __name__ == '__main__':

    SEED = 23092017
    MODELNAMES = ['resnet'] # ['alexnet', 'vgg', 'googlenet', 'resnet']
    OUTDIR = 'results'
    DATASETNAMES = ['HeadLeglessB', 'FullF', 'FullB', 'HeadLegArmlessF', 'HeadLegArmlessB'] # ['HeadLegArmlessB', 'HeadLegArmlessF', 'HeadlessB', 'HeadlessF', 'OriginalB', 'OriginalF', 'FullB', 'FullF', 'HeadLeglessB', 'HeadLeglessF']
    EPOCHS =16
    BATCH_SIZE = 16
    OPTIMIZERS = ['adam'] # ['sgd', 'adam', 'rmsprop']
    LEARNING_RATE = 0.0001
    CUDA = True
    GPU_ID = 0
    VERBOSE = True
    NB_FOLDS = 10

    print('Building paramsets...')

    paramsets = []
    for j in range(len(MODELNAMES)):
        for k in range(len(DATASETNAMES)):
            for w in range(len(OPTIMIZERS)):
                for z in range(NB_FOLDS):
                    args = types.SimpleNamespace()
                    args.model                    = MODELNAMES[j]
                    args.dataset                  = DATASETNAMES[k]
                    args.output_dir               = OUTDIR
                    args.epochs                   = EPOCHS
                    args.batch_size               = BATCH_SIZE
                    args.optimizer                = OPTIMIZERS[w]
                    args.learning_rate            = LEARNING_RATE
                    args.momentum                 = 0.9
                    args.weight_decay             = 0.0005
                    args.augment_all              = False
                    args.augment_mining           = True
                    args.augment_mining_min_bound = 10.
                    args.augment_mining_max_bound = 18.
                    args.augment_hflip            = True
                    args.augment_noise            = False
                    args.gpu_id                   = GPU_ID
                    args.use_cuda                 = CUDA
                    args.save_ckpt                = False
                    args.verbose                  = VERBOSE
                    args.nb_folds                 = NB_FOLDS
                    args.fold_offset              = z
                    paramsets.append(args)

    print('START')
    run(paramsets)
