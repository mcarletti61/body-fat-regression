# -*- coding: utf-8 -*-

import argparse

import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms

from FATDATA import FATDATA

import matplotlib.pyplot as plt
import pickle

if __name__ == '__main__':

    # !!! RUN THIS SCRIPT FROM PARENT FOLDER !!!

    seed = 23092017
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    # Parse arguments.
    parser = argparse.ArgumentParser()

    parser.add_argument('--dataset', type=str, default='HeadLeglessF',
                        help='dataset name; must be in [\'HeadLegArmlessB\', \'HeadLegArmlessF\', \'HeadlessB\', \'HeadlessF\', \'OriginalB\', \'OriginalF\', \'FullB\', \'FullF\', \'HeadLeglessB\', \'HeadLeglessF\']')

    args = parser.parse_args()

    # Normalize RGB-8 images between -1 and +1.
    transf = transforms.Compose([transforms.Scale((224,224)),
                                transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    trainset = FATDATA(name=args.dataset, train=True, transform=transf)
    testset = FATDATA(name=args.dataset, train=False, transform=transf)

    gtval = trainset.values
    N = gtval.size

    xx = np.linspace(0,N-1,N)
    tr = np.array([None] * N)
    tr[trainset.train_idx] = gtval[trainset.train_idx]
    ts = np.array([None] * N)
    ts[testset.test_idx] = gtval[testset.test_idx]

    idx = np.argsort(gtval).squeeze()
    gtval = gtval[idx]

    tr = tr[idx].tolist()
    ts = ts[idx].tolist()

    with open('./data/FatnetBodyScan/best_split_if_sorted.pkl', 'wb') as fp:
        pickle.dump([tr, ts], fp, pickle.HIGHEST_PROTOCOL)

    plt.figure()
    plt.plot(xx,tr,color='g',label='train')
    plt.scatter(xx,ts,color='b',label='test')
    plt.title('Train/Test distribution')
    plt.legend()
    plt.grid(True)
    plt.show()
