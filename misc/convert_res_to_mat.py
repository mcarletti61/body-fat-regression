import pickle
import glob
import os
import scipy.io as sio

def convert_pickle_to_mat(pklfile):
    '''Convert to mat the input pickle file assuming its content is a dictionary.
    The destination folder is the same of the pickle file'''
    with open(pklfile, 'rb') as fp:
        data = pickle.load(fp)
    parent = os.path.dirname(pklfile)
    matfile = os.path.basename(pklfile).split('.')[0] + '.mat'
    sio.savemat(os.path.join(parent, matfile), data, do_compression=True)

if __name__ == '__main__':

    # !!! RUN THIS SCRIPT FROM PARENT FOLDER !!!

    fpaths = glob.glob('./results/*')
    for p in fpaths:
        pklfile = os.path.join(p, 'results.pickle')
        if os.path.isdir(p) and os.path.exists(pklfile):
            convert_pickle_to_mat(pklfile)
